# README #

This repository is used to track template file changes between phpMyDirectory versions.

Changes between 1.5.2 and 1.5.3:
https://bitbucket.org/phpmydirectory/phpmydirectory-templates/branches/compare/master%0D1.5.2#diff

Changes between 1.5.1 and 1.5.3:
https://bitbucket.org/phpmydirectory/phpmydirectory-templates/branches/compare/master%0D1.5.1#diff

Changes between 1.5.0 and 1.5.3:
https://bitbucket.org/phpmydirectory/phpmydirectory-templates/branches/compare/master%0D1.5.0#diff

For support, please contact:
support@phpmydirectory.com